package com.epam.transaction.source;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public abstract class Database {
	static {
        try {
            DriverManager.registerDriver(new oracle.jdbc.OracleDriver());
        } catch (SQLException ex) {
            throw new ExceptionInInitializerError();
        }
    }

    public static Connection getConnection() {
        Connection db;
        try {            
            Properties properties = new Properties();
            properties.setProperty("user", "majoode");
            properties.setProperty("password", "carrera4");
            
            db = DriverManager.getConnection("jdbc:oracle:thin:@//localhost:1521/xe", properties);
        } catch (SQLException ex) {
            throw new RuntimeException(ex.getSQLState() + ex.getMessage());
        }
        return db;
    }
}
