package com.epam.transaction.handler;

import java.sql.Connection;
import java.sql.SQLException;

import com.epam.transaction.source.Database;

public class SourceUtil {

	private static ConnectionHolder holder = new ConnectionHolder();
	private static boolean withinTransaction = false;

	public static boolean isWithinTransaction() {
		return withinTransaction;
	}

	static void beginTransaction(boolean withinTransaction) {
		holdConnection();
		SourceUtil.withinTransaction = withinTransaction;
	}

	public static Connection aquireConnection() {
		Connection con = holder.getConnection();
		if (con != null) {
			return con;
		} else {
			holdConnection();
			return holder.getConnection();
		}
	}

	static void holdConnection() {
		try {
			Connection con = Database.getConnection();
			if (withinTransaction) {
				con.setAutoCommit(false);
			}
			holder.setConnection(con);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	static void rollback() {
		try {
			holder.getConnection().rollback();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public static void releaseConnection() {
		if (!withinTransaction) {
			try {
				holder.getConnection().close();
				holder.removeConnection();
			} catch (SQLException e) {
				throw new RuntimeException(e);
			}
		}
	}

	static void endUpTransaction() {
		try {
			Connection con = holder.getConnection();
			con.setAutoCommit(true);
			con.close();
			holder.removeConnection();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		withinTransaction = false;
	}
}
