package com.epam.transaction.handler;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

import com.epam.transaction.annotation.TransactionalAction;

public class DatabaseTransactionManager<T> implements InvocationHandler {

	private T service;

	public DatabaseTransactionManager(T service) {
		super();
		this.service = service;
	}

	@SuppressWarnings("unchecked")
	public static <T> T newInstance(T service, Class<?>... intf) {
		return (T) Proxy.newProxyInstance(service.getClass().getClassLoader(), intf,
				new DatabaseTransactionManager<T>(service));
	}

	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		Object result = null;

		if (method.getDeclaredAnnotation(TransactionalAction.class) != null) {
			SourceUtil.beginTransaction(true);
			try {
				result = method.invoke(service, args);
			} catch (Exception e) {
				SourceUtil.rollback();
				throw e;
			} finally {
				SourceUtil.endUpTransaction();
			}
		} else {
			try {
				SourceUtil.holdConnection();
				result = method.invoke(service, args);
			} catch (Exception ex) {
				throw ex;
			} finally {
				SourceUtil.endUpTransaction();
			}
		}
		return result;
	}

}
