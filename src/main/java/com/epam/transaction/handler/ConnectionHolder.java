package com.epam.transaction.handler;

import java.sql.Connection;

public class ConnectionHolder {
	
	private ThreadLocal<Connection> con = new ThreadLocal<>();
	
	public Connection getConnection(){
		return con.get();
	}
	
	public void setConnection(Connection connection){
		con.set(connection);
	}
	
	public void removeConnection(){
		con.remove();
	}
}
