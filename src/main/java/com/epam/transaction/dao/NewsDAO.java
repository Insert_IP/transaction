package com.epam.transaction.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.epam.transaction.domain.News;
import com.epam.transaction.handler.SourceUtil;

public class NewsDAO implements DAO {
	private final String FETCH = "SELECT * FROM NEWS";
	private final String FETCH_ERROR = "SELECT * FROM NEW";
	private final String INSERT_ARRIVAL = "INSERT INTO NEWS(NWS_NEWS_ID, NWS_TITLE, NWS_SHORT_TEXT, NWS_FULL_TEXT, NWS_CREATION_DATE, NWS_MODIFICATION_DATE) VALUES(NWS_SEQ.NEXTVAL, ?, ?, ?, ?, ?)";

	@Override
	public Long insert(News news) {
		Long id = null;
		Connection con = SourceUtil.aquireConnection();
		try (PreparedStatement ps = con.prepareStatement(INSERT_ARRIVAL, new String[] { "NWS_NEWS_ID" })) {

			ps.setString(1, news.getTitle());
			ps.setString(2, news.getSht_text());
			ps.setString(3, news.getText());
			ps.setTimestamp(4, Timestamp.valueOf(news.getCreation()));
			ps.setTimestamp(5, Timestamp.valueOf(news.getModification()));
			ps.executeUpdate();

			ResultSet rs = ps.getGeneratedKeys();

			while (rs.next()) {
				id = rs.getLong(1);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			SourceUtil.releaseConnection();
		}
		return id;
	}

	@Override
	public List<News> fetchAll() {
		List<News> list = null;
		Connection con = SourceUtil.aquireConnection();
		try (PreparedStatement ps = con.prepareStatement(FETCH)) {
			ResultSet rs = ps.executeQuery();

			list = new ArrayList<>();

			while (rs.next()) {
				News news = new News();
				news.setId(rs.getInt(1));
				news.setTitle(rs.getString(2));
				news.setSht_text(rs.getString(3));
				news.setText(rs.getString(4));
				news.setCreation(rs.getTimestamp(5).toLocalDateTime());
				news.setModification(rs.getTimestamp(6).toLocalDateTime());
				list.add(news);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			SourceUtil.releaseConnection();
		}
		return list;
	}

	@Override
	public List<News> fetchAllError() {
		List<News> list = null;
		Connection con = SourceUtil.aquireConnection();
		try (PreparedStatement ps = con.prepareStatement(FETCH_ERROR)) {
			ResultSet rs = ps.executeQuery();

			list = new ArrayList<>();

			while (rs.next()) {
				News news = new News();
				news.setId(rs.getInt(1));
				news.setTitle(rs.getString(2));
				news.setSht_text(rs.getString(3));
				news.setText(rs.getString(4));
				news.setCreation(rs.getTimestamp(5).toLocalDateTime());
				news.setModification(rs.getTimestamp(6).toLocalDateTime());
				list.add(news);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			SourceUtil.releaseConnection();
		}

		return list;
	}
}
