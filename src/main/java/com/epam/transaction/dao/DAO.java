package com.epam.transaction.dao;

import java.util.List;

import com.epam.transaction.domain.News;

public interface DAO {
	Long insert(News news);
	
	List<News> fetchAll();
	
	List<News> fetchAllError();
}
