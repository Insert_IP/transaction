package com.epam.transaction.domain;

import java.io.Serializable;
import java.time.LocalDateTime;

public class News implements Serializable {

	private static final long serialVersionUID = -7388211207362570811L;

	private long id;

	private String title;

	private String sht_text;

	private String text;

	private LocalDateTime creation;

	private LocalDateTime modification;

	private long nextId;

	private long prevId;

	private int version;

	public News() {

	}

	public News(long id, String title, String sht_text, String text, LocalDateTime creation, LocalDateTime modification,
			long nextId, long prevId) {
		super();
		this.id = id;
		this.title = title;
		this.sht_text = sht_text;
		this.text = text;
		this.creation = creation;
		this.modification = modification;
		this.nextId = nextId;
		this.prevId = prevId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getSht_text() {
		return sht_text;
	}

	public void setSht_text(String sht_text) {
		this.sht_text = sht_text;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public LocalDateTime getCreation() {
		return creation;
	}

	public void setCreation(LocalDateTime creation) {
		this.creation = creation;
	}

	public LocalDateTime getModification() {
		return modification;
	}

	public void setModification(LocalDateTime modification) {
		this.modification = modification;
	}

	public long getNextId() {
		return nextId;
	}

	public void setNextId(long nextId) {
		this.nextId = nextId;
	}

	public long getPrevId() {
		return prevId;
	}

	public void setPrevId(long prevId) {
		this.prevId = prevId;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((creation == null) ? 0 : creation.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((modification == null) ? 0 : modification.hashCode());
		result = prime * result + (int) (nextId ^ (nextId >>> 32));
		result = prime * result + (int) (prevId ^ (prevId >>> 32));
		result = prime * result + ((sht_text == null) ? 0 : sht_text.hashCode());
		result = prime * result + ((text == null) ? 0 : text.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		News other = (News) obj;
		if (creation == null) {
			if (other.creation != null)
				return false;
		} else if (!creation.equals(other.creation))
			return false;
		if (id != other.id)
			return false;
		if (modification == null) {
			if (other.modification != null)
				return false;
		} else if (!modification.equals(other.modification))
			return false;
		if (nextId != other.nextId)
			return false;
		if (prevId != other.prevId)
			return false;
		if (sht_text == null) {
			if (other.sht_text != null)
				return false;
		} else if (!sht_text.equals(other.sht_text))
			return false;
		if (text == null) {
			if (other.text != null)
				return false;
		} else if (!text.equals(other.text))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "News [id=" + id + ", title=" + title + ", sht_text=" + sht_text + ", text=" + text + ", creation="
				+ creation + ", modification=" + modification + "]";
	}

}
