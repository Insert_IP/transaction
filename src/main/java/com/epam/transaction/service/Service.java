package com.epam.transaction.service;

import com.epam.transaction.annotation.TransactionalAction;

public interface Service {
	
	@TransactionalAction
	void fetchNews();
}
