package com.epam.transaction.service;

import java.time.LocalDateTime;

import com.epam.transaction.dao.DAO;
import com.epam.transaction.dao.NewsDAO;
import com.epam.transaction.domain.News;
import com.epam.transaction.handler.DatabaseTransactionManager;

public class NewsService implements Service {
	
	DAO dao = new NewsDAO();
	
	private NewsService() {
		super();
	}
	
	public static Service getInstance(boolean proxied){
		if(proxied){
			return DatabaseTransactionManager.newInstance(new NewsService(), NewsService.class.getInterfaces());
		} else {
			return new NewsService();
		}
	}

	public void fetchNews(){
		News news = new News(1, "title", "Short text", "Text", LocalDateTime.now(), LocalDateTime.now(), 0, 2);
		System.out.println(dao.insert(news));
		System.out.println(dao.fetchAllError());
	}
}
