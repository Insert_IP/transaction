package com.epam.transaction;

import com.epam.transaction.service.NewsService;
import com.epam.transaction.service.Service;

public class App 
{
    public static void main(String[] args ) throws NoSuchMethodException, SecurityException
    {
		Service service = NewsService.getInstance(true);
		service.fetchNews();
    }
}
